FROM sameersbn/ubuntu
MAINTAINER Sam Giles

RUN apt-get update
RUN apt-get  install -y xvfb x11vnc x11-xkb-utils xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic x11-apps


RUN apt-key adv --keyserver keyserver.ubuntu.com --recv 7212620B \
 && echo "deb http://archive.canonical.com/ trusty partner" >> /etc/apt/sources.list \
 && dpkg --add-architecture i386 \
 && apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y pulseaudio:i386 \
 && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y libgl1-mesa-glx:i386 sssd:i386


RUN apt-get update && apt-get install -y gdebi-core
RUN wget http://media.steampowered.com/client/installer/steam.deb
RUN gdebi steam.deb
#RUN apt-get update && apt-get install -y steam:i386
RUN apt-get update && apt-get install -y libnss-sss:i386
RUN useradd -ms /bin/bash app

ENV DISPLAY :99

ADD xvfb_init /etc/init.d/xvfb
RUN chmod a+x /etc/init.d/xvfb
ADD xvfb_daemon_run /usr/bin/xvfb-daemon-run
RUN chmod a+x /usr/bin/xvfb-daemon-run
RUN mkdir /var/run/xvfb
RUN chown app:app /var/run/xvfb/
RUN mkdir -p "/home/app/.local/share/Paradox Interactive/Stellaris"
RUN chown app:app "/home/app/.local/share/Paradox Interactive/Stellaris"

ADD run_stellaris.sh /home/app/run_stellaris.sh
RUN chmod a+x /home/app/run_stellaris.sh

USER app
WORKDIR /home/app/stellaris
CMD ["../run_stellaris.sh"]
#CMD ["xvfb-daemon-run", "./stellaris", "-nolauncher"]
